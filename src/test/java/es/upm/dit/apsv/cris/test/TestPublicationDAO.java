package es.upm.dit.apsv.cris.test;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import es.upm.dit.apsv.cris.dao.PublicationDAO;
import es.upm.dit.apsv.cris.dao.PublicationDAOImplementation;
import es.upm.dit.apsv.cris.dataset.CSV2DB;
import es.upm.dit.apsv.cris.model.Publication;

			
class TestPublicationDAO {
	
	private Publication p;
	private PublicationDAO pdao;

	@BeforeAll
	
	static void dbSetUp() throws Exception {
		CSV2DB.loadPublicationsFromCSV("publications.csv");
		CSV2DB.loadResearchersFromCSV("researchers.csv");
	}

	@BeforeEach
	
	void setUp() throws Exception {
		pdao = PublicationDAOImplementation.getInstance();
		p = new Publication();
		p.setId("34548108157");
		p.setTitle("Mechanical behavior of unidirectional fiber-reinforced polymers under transverse compression: Microscopic mechanisms and modeling");
		p.setPublicationName("Composites Science and Technology");
		p.setPublicationDate("2007-10-01");
		p.setAuthors("7403372571;55708653400");
		p.setCiteCount(0);
	}
/*
 	@Test
	void testUpdate() {
	        String oldt = p.getTitle();
	        p.setTitle("1111");
	        pdao.update(p);
	        assertEquals(p, pdao.read(p.getId()));
	        p.setTitle(oldt);
	        pdao.update(p);
	}
	@Test
	void testCreate() {
	        pdao.delete(p);
	        pdao.create(p);
	        assertEquals(p, pdao.read(p.getId()));
	}

	@Test
	void testRead() {
	        assertEquals(p, pdao.read(p.getId()));
	}

	
*/
	

	@Test
	void testReadAll() {
	        assertTrue(pdao.readAll().size() > 900);
	}
	
	@Test
	void testDelete() {
	        pdao.delete(p);
	        assertNull(pdao.read(p.getId()));
	        pdao.create(p);
	}
	

}

